using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	public class StarterAssetsInputs : MonoBehaviour
	{
		[Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool sprint;

		[Header("Movement Settings")]
		public bool analogMovement;

		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED

		Gun gunScript;
		UIManager uiManager;
		GameManager gameManager;

		private void Start()
		{
			gunScript = GameObject.Find("PlayerCapsule").GetComponent<Gun>();
			uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
			gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		}


		public void OnLoadLevel()
		{
			Debug.Log("Load level");
			gameManager.LoadLevel();
		}

		public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnJump(InputValue value)
		{
			JumpInput(value.isPressed);
		}

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}

		public void OnShoot()
		{
			Debug.LogWarning("Warning");
			Debug.LogError("Error");
			Debug.Log("Shoot!");
			gunScript.ShootMethod();
		}

		public void OnExplode()
		{
			Debug.Log("Explode!");
			gunScript.ExplodeMethod();
		}
		
		public void OnToggleMenu()
		{
			Debug.Log("Toggle menu");
			uiManager.ToggleMenu();
		}

#endif


		public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			jump = newJumpState;
		}

		public void SprintInput(bool newSprintState)
		{
			sprint = newSprintState;
		}
		
		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}
	}
	
}