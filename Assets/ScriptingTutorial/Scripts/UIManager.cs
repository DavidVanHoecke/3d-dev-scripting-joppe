using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    CanvasGroup uiCanvas;

    // Start is called before the first frame update
    void Start()
    {
        uiCanvas.alpha = 0;
    }

    public void ToggleMenu()
    {
        if(uiCanvas.alpha == 1)
        {
            uiCanvas.alpha = 0;
            Cursor.visible = false;
        }
        else
        {
            uiCanvas.alpha = 1;
            Cursor.visible = true;  
        }
    }



}
