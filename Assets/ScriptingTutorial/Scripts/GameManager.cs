﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int points;

    void Start()
    {
        points = 0;
    }

    public void LoadLevel()
    {

        SceneManager.LoadScene("GameOver");
    }
}
