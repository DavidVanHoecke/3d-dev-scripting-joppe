﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Gun : MonoBehaviour
{
    [SerializeField]
    GameObject bulletPrefab;

    float time;

    float coolDownTime = 0.2f;

    bool bulletMustCoolDown = false;

    Bullet currentBullet;

    void Start()
    {
        time = 0;
    }

    private void Update()
    {

        if (bulletMustCoolDown)
        {
            time += Time.deltaTime;
            if (time >= coolDownTime)
            {
                bulletMustCoolDown = false;
            }
        }
    }

    //SHOOT
    public void ShootMethod()
    {
        if (!bulletMustCoolDown)
        {
            currentBullet = ShootBullet();

            bulletMustCoolDown = true;
            time = 0;
        }
    }

    //EXPLODE
    public void ExplodeMethod()
    {
        Debug.Log("EXPLODE");
        if (currentBullet)
        {
            currentBullet.Explode();
        }
    }

    private Bullet ShootBullet()
    {
        GameObject newBullet = GameObject.Instantiate(bulletPrefab, this.transform);
        newBullet.transform.parent = null;
        newBullet.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.forward * 500);

        return newBullet.GetComponent<Bullet>();
    }
}
