﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject zombiePrefab;

    float time;

    [SerializeField]
    float zombieSpawnTimeout = 3;

    // Start is called before the first frame update
    void Start()
    {
        SpawnZombie();

         time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if(time >= zombieSpawnTimeout)
        {
            SpawnZombie();

            time = 0;
        }
    }

    private void SpawnZombie()
    {
        float x = Random.Range(-12, 12);
        float z = Random.Range(-12, 12);

        Vector3 spawnPosition = new Vector3(x, 0, z);

        Debug.Log("Spawn zombie");

        //SPAWN ZOMBIE HERE
        Instantiate(zombiePrefab, spawnPosition, Quaternion.identity);
    }
}
